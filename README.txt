--------------------------------------------------------------------------------
  comment_form_display module Readme
  https://www.drupal.org/project/comment_form_display
--------------------------------------------------------------------------------

Contents:
=========
1. INTRODUCTION
2. INSTALLATION
3. REQUIREMENTS
4. CONFIGURATION
5. CREDITS

1. INTRODUCTION
========

This module allows you to manage which display to use for comments.
Currently, you can choose between standard mode or AJAX mode. 
This latest mode let you update your comment without redirection or reloading the page.

2. INSTALLATION
===============

Install as usual, see http://drupal.org/node/70151 for further information.

3. REQUIREMENTS
===============

This module requires :
Module CTools - http://drupal.org/project/ctools

This module is optimized to work with Bootstrap.

4. CONFIGURATION
================

The simplest way is :
1. Install modules comment_form_display.
2. Go to the settings page (/admin/config/content/comment-display) and choose which display you want for each actions.
3. Clear the cache.

You can now add comments on contents and try it.

5. CREDITS
==========

Project page: http://drupal.org/project/comment_form_display

Maintainer:
Maxime Lebon - https://www.drupal.org/u/guish1
